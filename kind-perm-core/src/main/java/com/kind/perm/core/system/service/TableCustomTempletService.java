package com.kind.perm.core.system.service;

import java.util.List;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.system.domain.TableCustomDO;
import com.kind.perm.core.system.domain.TableCustomTempletDO;

public interface TableCustomTempletService {


	
	 /**
     * 分页查询
     * @param pageQuery
     * @return
     */
	PageView<TableCustomTempletDO> selectPageList(PageQuery pageQuery);

    /**
     * 保存数据
     * @param entity
     */
	int save(TableCustomTempletDO entity);

    /**
     * 获取数据对象
     * @param id
     * @return
     */
	TableCustomTempletDO getById(Long id);

    /**
     * 删除数据
     * @param id
     */
	void remove(Long id);
	
	
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<TableCustomTempletDO> queryList(TableCustomTempletDO entity);
	
}
