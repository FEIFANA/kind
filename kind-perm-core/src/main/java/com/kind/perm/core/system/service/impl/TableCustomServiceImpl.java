package com.kind.perm.core.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.exception.ServiceException;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.demo.domain.CommunityDO;
import com.kind.perm.core.system.dao.TableCustomDao;
import com.kind.perm.core.system.domain.FileDO;
import com.kind.perm.core.system.domain.TableCustomDO;
import com.kind.perm.core.system.service.TableCustomService;

/**
 * 
 * 导出业务处理实现类. <br/>
 *
 * @date:2017年01月19日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class TableCustomServiceImpl implements TableCustomService {

	@Autowired
	private TableCustomDao tableCustomDao;

	@Override
	public List<TableCustomDO> findTableCustomExport(Long type) {
		
		return tableCustomDao.findTableCustomExport(type);
	}

	@Override
	public PageView<TableCustomDO> selectPageList(PageQuery pageQuery) {
		pageQuery.setPageSize(pageQuery.getPageSize());
		List<TableCustomDO> list = tableCustomDao.page(pageQuery);
		int count = tableCustomDao.count(pageQuery);
        pageQuery.setItems(count);
		return new PageView<>(pageQuery, list);
	}

	@Override
	public int save(TableCustomDO entity) {
		try {
	           return tableCustomDao.saveOrUpdate(entity);
	        }catch (Exception e){
			    e.printStackTrace();
	            throw new ServiceException(e.getMessage());
	        }
	}

	@Override
	public TableCustomDO getById(Long id) {
		return tableCustomDao.getById(id);
	}

	@Override
	public void remove(Long id) {
		try {
			tableCustomDao.remove(id);
        }catch (Exception e){
            throw new ServiceException(e.getMessage());
        }
		
	}

	@Override
	public List<TableCustomDO> queryList(TableCustomDO entity) {
		return tableCustomDao.queryList(entity);
	}
	
	
}
