/**
 * Project Name:kafa-wheat-core
 * File Name:UserRoleDao.java
 * Package Name:com.kind.perm.core.dao
 * Date:2016年6月14日下午5:15:06
 * Copyright (c) 2016, http://www.mcake.com All Rights Reserved.
 *
*/

package com.kind.perm.core.system.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.system.dao.TableCustomTempletDao;
import com.kind.perm.core.system.domain.TableCustomTempletDO;

/**
 * Function:导出数据模板持久化接口. <br/>
 * Date: 2017年02月04日 上午10:15:06 <br/>
 * 
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
@Repository
public class TableCustomTempletDaoImpl extends BaseDaoMyBatisImpl<TableCustomTempletDO, Serializable>
implements TableCustomTempletDao{


	@Override
	public List<TableCustomTempletDO> page(PageQuery pageQuery) {
		return super.query(NAMESPACE + "page", pageQuery);
	}

	@Override
	public int count(PageQuery pageQuery) {
		return super.count(NAMESPACE + "count", pageQuery);
	}

	@Override
	public int saveOrUpdate(TableCustomTempletDO entity) {
		if (entity.getId() == null) {
			return super.insert(NAMESPACE + "insert", entity);
		} else {
			return super.update(NAMESPACE + "update", entity);
		}
	}

	@Override
	public TableCustomTempletDO getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}

	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
		
	}

	@Override
	public List<TableCustomTempletDO> queryList(TableCustomTempletDO entity) {
		 return super.query(NAMESPACE + "queryList", entity);
	}
	

}
