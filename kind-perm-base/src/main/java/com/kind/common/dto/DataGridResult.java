package com.kind.common.dto;

import java.util.List;

/**
 * DataGrid的数据模型封装. <br/>
 * Date: 2016年6月28日 下午7:19:21 <br/>
 * 
 * @version
 * @since JDK 1.7
 * @see
 */
public class DataGridResult {

	public DataGridResult() {
		super();
	}

	public DataGridResult(List<?> rows, Long total) {
		super();
		this.rows = rows;
		this.total = total;
	}

	/**
	 * 当前页记录数
	 */
	private List<?> rows;
	/**
	 * 总数
	 */
	private Long total;

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

}
